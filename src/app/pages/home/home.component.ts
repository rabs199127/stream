import { TOUCH_BUFFER_MS } from '@angular/cdk/a11y/input-modality/input-modality-detector';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { concat, from, fromEvent } from 'rxjs';
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, flatMap, map, mergeMap, takeWhile, tap, toArray } from 'rxjs/operators';
import { Movie, NameValue } from 'src/app/shared/models/models';
import { ApiService } from '../../services/api.service';

// import { debug } from 'console';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	pageIndex: number = 0;
	pageCount: number = 10;
	selectOptions: Array<NameValue> = [
		{
			name: 'Ambas',
			value: 'ambas'
		},
		{
			name: 'Series',
			value: 'series'
		},
		{
			name: 'Peliculas',
			value: 'movie'
		}
	];
	selectedCategory:string = 'ambas';	
	movies$: Observable<Movie[]>;

	@ViewChild('filterByReleaseYearInput', { static: false }) filterByReleaseYearInput: ElementRef;

	constructor(
		private _apiServeice: ApiService
	) { }

	ngOnInit(): void {
		this.movies$ = this._apiServeice.getMovies(this.pageIndex, this.pageCount, 'ambas');
	}

	ngAfterViewInit() {
		this.setKeyUpFiltersInputsEvents()
	}
	
	setKeyUpFiltersInputsEvents(){
		fromEvent(this.filterByReleaseYearInput.nativeElement, 'keyup')
            .pipe(
                debounceTime(150),
                distinctUntilChanged(),
                tap(()=>this.filterByReleaseYear())
            )
            .subscribe();
	}

	getMore() {
		this.pageCount += 10; 
		this.movies$ = this._apiServeice.getMovies(this.pageIndex, this.pageCount, this.selectedCategory);
	}

	sort(sortBy: string) {
		switch (sortBy) {
			case 'releaseYear':
				this.movies$ = this.movies$.pipe(map(data => data.sort(this.sortByReleaseYear)))
				break;
			default:
			case 'title':
				this.movies$ = this.movies$.pipe(map(data => data.sort(this.sortByTitle)))
				break;

		}
	}

	sortByTitle = (a: Movie, b: Movie) => {
		const titleA = a.title.toLocaleUpperCase();
		const titleB = b.title.toLocaleUpperCase();
		return (titleA < titleB) ? -1 : (titleA > titleB) ? 1 : 0;
	}

	sortByReleaseYear = (a: Movie, b: Movie) => {
		return b.releaseYear - a.releaseYear;
	}

	filterByReleaseYear() {
		if (this.filterByReleaseYearInput.nativeElement.value != "") { 
			this.movies$ = this.movies$.pipe(mergeMap(data=>data),
			filter(movie => movie.releaseYear > 1895 && movie.releaseYear == this.filterByReleaseYearInput.nativeElement.value),
			toArray());

		} else {
			this.movies$ = this._apiServeice.getMovies(this.pageIndex, this.pageCount, this.selectedCategory)
		}
    }

	filterByCategory(event:MatSelectChange){		
		this.movies$ = this._apiServeice.getMovies(this.pageIndex, this.pageCount, event.value);
	}

	onScrollDown(ev: any) {
		this.getMore();
	}
}


