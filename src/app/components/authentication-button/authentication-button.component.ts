import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';

@Component({
	selector: 'app-authentication-button',
	templateUrl: './authentication-button.component.html',
	styleUrls: ['./authentication-button.component.scss']
})
export class AuthenticationButtonComponent implements OnInit {

	constructor(
		public auth: AuthService,
		@Inject(DOCUMENT) private doc: Document
	) { }


	ngOnInit(): void {
		
	}

	login() {

		this.auth.loginWithRedirect({
			appState: { target: '/home' }
		});
	}

	logout(): void {
		this.auth.logout({ returnTo: this.doc.location.origin });
	}

}
