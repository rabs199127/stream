export class Movie {
	description:string;
	images:any;
	programType: string;
	releaseYear:number;
	title:string;
}

export class PosterArt {
	height: number;
	url: string;
	width: number;
}

export class MoviesTotalCount {
    total: number;
    entries:Movie[];
}

export class NameValue {
    name:string;
    value:string
}