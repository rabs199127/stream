import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { filter, skip, take, toArray } from 'rxjs/operators';
import moviesData from '../api/movies.json';
import { Movie, MoviesTotalCount } from '../shared/models/models';

@Injectable({
	providedIn: 'root'
})
export class ApiService {

	movies: MoviesTotalCount = moviesData;

	constructor() { }

	public getMovies(pageIndex: number, pagesCount: number, filterBy: string): Observable<Movie[]> {
		let movies$ = from(this.movies.entries);

		switch (filterBy) {
			case 'series':
			case 'movie':
				movies$ = movies$.pipe(skip(0), take(pagesCount), filter(movie => movie.programType === filterBy));
				break;
			default:
				movies$ = movies$.pipe(skip(0), take(pagesCount));
				break;
		}

		return movies$.pipe(toArray());
	}


}
