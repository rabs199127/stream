import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AuthModule } from '@auth0/auth0-angular';

import { HomeComponent } from './pages/home/home.component';
import { AuthenticationButtonComponent } from './components/authentication-button/authentication-button.component';
import { environment } from 'src/environments/environment';
import { LoginComponent } from './pages/login/login.component';

//Material modules
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';

//External libraries
import { InfiniteScrollModule } from 'ngx-infinite-scroll';



@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		AuthenticationButtonComponent,
		LoginComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		AuthModule.forRoot({
			...environment.auth
		}),
		//material modules
		MatToolbarModule,
		MatProgressSpinnerModule,
		MatInputModule,
		MatSelectModule,
		MatCardModule,
		MatButtonModule,

		//external libraries
		InfiniteScrollModule 
		
	],
	providers: [
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
