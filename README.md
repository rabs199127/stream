# Challenge

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.1.


## Requerimientos
Node.js 12.x o superior

## Instalacion
Clonar el repositorio
Situarse en le carpeta raiz del proyecto y ejecutar `npm install`
Ejecutar `npm start`

## Importante
El Responsive no esta completo
La primera vez que se realice un login con la aplicacion, pedira permisos para el Auth0


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

